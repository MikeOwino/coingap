import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table, Spin } from 'antd';

const TopCoins = () => {
  const [coins, setCoins] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get('https://api.coincap.io/v2/assets');
      setCoins(result.data.data.slice(0, 100));
      setLoading(false);
    };

    fetchData();

    const intervalId = setInterval(() => {
      fetchData();
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);

  const columns = [
    {
      title: 'Coin Name',
      dataIndex: 'name',
    },
    {
      title: 'Price',
      dataIndex: 'priceUsd',
    },
  ];

  return (
    <div style={{ textAlign: 'center', padding: '50px' }}>
      {loading ? (
        <Spin size="large" />
      ) : (
        <Table dataSource={coins} columns={columns} />
      )}
    </div>
  );
};

export default TopCoins;
